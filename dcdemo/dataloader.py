"""
Populate data structures from the external files
"""
__author__ = "Richard Shea (rshea@thecubagroup.com)"
__version__ = "$Revision: 0.1 $"
__date__ = "$Date: 2013/08/18 10:00:00 $"
__copyright__ = "Copyright (c) 2013 Richard Shea"

from django.conf import settings
def makeDatabasesSetting():
    '''
    Set up database configuration for this standalone script
    '''

    DBPATH = '''/home/rshea/dev/djchdemo/db/djchdemo.db'''
    MYDB = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3', 
            'NAME': DBPATH, 
            'USER': '',
            'PASSWORD': '',
            'HOST': '',   
            'PORT': '',  
        }
    }
    settings.configure(DATABASES=MYDB)

import pprint
import csv
makeDatabasesSetting()
from dcdemo.apps.dmodrv.models import Country
from dcdemo.apps.dmodrv.models import City   

from django.conf import settings
from decimal import Decimal

COUNTRYDATAPATH = """/home/rshea/dev/djchdemo/data/wikipedia-iso-country-codes.csv"""
CITYDATAPATH = """/home/rshea/dev/djchdemo/data/worldcitiespop.txt"""
COUNTRIESTOPROCESS = ['au', 'AU', 'cn', 'CN', 'nz', 'NZ' ,'us', 'US']
def removeLineFeed(line):
    '''
    Removes line feeds from the argument supplied
    '''
    line = line.replace("\r","")
    line = line.replace("\n","")
    return line

def processCountryData(lineFromCountryDataFile, notUsed):
    '''
    Expects that the 'lineFromCountryDataFile' is two
    element string delimited by a semi-colon. The first 
    element is expected to be Country Name; the second
    element is expected to an ISO country code. Both
    elements are used to instantiate an instance of 
    Country and this is returned to the caller
    '''
    lstData = lineFromCountryDataFile.split(":")
    iso = lstData[-1]
    lstData = lstData[0].split(",")
    name = lstData[0]
    if iso in COUNTRIESTOPROCESS:
        c = Country(name=name.title(), isocode=iso.lower())
        c.save()

def populateAbstract(pathToData, lineProcessingFunction, lpfArg0=None):
    '''
    Opens file on specified path, ignores first line and then
    iterates over all others invoking the 'lineProcessingFunction'
    function each time with the line of input as an argument to it.
    
    The returned objects are saved into a list and at the end of
    processing the input this list is returned to the caller
    '''
    lstOut = []
    firstLine = True
    for line in open(pathToData):
        if firstLine:
            firstLine = False
        else:
            line = removeLineFeed(line)
            if line == None or len(line.strip()) < 1:
                pass
            else:
                lstOut.append(lineProcessingFunction(line, lpfArg0))
    return lstOut

def populateCountries(pathToCountryData):
    '''
    Invokes populateAbstract to process the input data
    defined in the file at 'pathToCountryData'
    '''
    populateAbstract(pathToCountryData, processCountryData)

def populateCities(pathToCityData):
    '''
    Invokes populateAbstract to process the input data
    defined in the file at 'pathToCityData'
    '''

    #Produce a list of City objects based upon the data contained
    #in the file at pathToCityData
    lstOut = []
    with open(pathToCityData, 'rb') as f:
        reader = csv.reader(f)
        dicReader = csv.DictReader(f)
        for dicRowFromCityDataFile in dicReader:
            if dicRowFromCityDataFile['Country'] not in COUNTRIESTOPROCESS:
                pass
            else:
                try:
                    lstOut.append(processCityData(dicRowFromCityDataFile))
                    if lstOut[-1] == None:
                        del lstOut[-1]
                    if len(lstOut) % 25000 == 0:
                        print "Up to %d" % len(lstOut)
                except csv.Error, e:
                    sys.exit('file %s, line %d: %s' % (filename, reader.line_num, e))
    
    return lstOut

def processCityData(dicRowFromCityDataFile):
    '''
    Expects that the 'dicRowFromCityDataFile' is a dictionary with
    the following elements.

    Country
    City
    AccentCity
    Region
    Population
    Latitude
    Longitude

    All elements apart from AccentCity are used to instantiate
    an instance of City and this is returned to the caller

    This 'dicCountries' argument is a dictionary of Country objects
    keyed by the ISO code for the relevant country

    This function returns a City object
    '''
    if dicRowFromCityDataFile['Country'] not in COUNTRIESTOPROCESS:
        pass
    else:
        coulist = Country.objects.filter(isocode=dicRowFromCityDataFile['Country'])
        cou = coulist[0]
        skipit = False
        try:
            if dicRowFromCityDataFile['Population'] == "":
                pop = None
            else:
                pop=int(dicRowFromCityDataFile['Population'])
        except:
            skipit = True
            print "Skipping for population"
        try:
            lng=Decimal(dicRowFromCityDataFile['Longitude'])
        except:
            skipit = True
            print "Skipping for Longitude"
            
        try:
            lat=Decimal(dicRowFromCityDataFile['Latitude'])
        except:
            skipit = True
            print "Skipping for Latitude"

        try:
            name=unicode(dicRowFromCityDataFile['City'])
        except:
            skipit = True
            print "Skipping for City Name"

        if not skipit:
            thisCity = City(country=cou, name=unicode(dicRowFromCityDataFile['City']), population=pop, \
                        longitude=Decimal(dicRowFromCityDataFile['Longitude']), latitude=Decimal(dicRowFromCityDataFile['Latitude']))
            thisCity.save()
        else:
            thisCity = None

        return thisCity

def main():
    lstCountries = populateCountries(COUNTRYDATAPATH)
    lstCities = populateCities(CITYDATAPATH) 
if __name__ == '__main__':
    main()
