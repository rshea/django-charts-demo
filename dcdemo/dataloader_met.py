"""
Populate data structures from the external files
"""
__author__ = "Richard Shea (rshea@thecubagroup.com)"
__version__ = "$Revision: 0.1 $"
__date__ = "$Date: 2013/08/23 19:00:00 $"
__copyright__ = "Copyright (c) 2013 Richard Shea"

from django.conf import settings
def makeDatabasesSetting():
    '''
    Set up database configuration for this standalone script
    '''

    DBPATH = '''/home/rshea/dev/djchdemo/db/djchdemo.db'''
    MYDB = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3', 
            'NAME': DBPATH, 
            'USER': '',
            'PASSWORD': '',
            'HOST': '',   
            'PORT': '',  
        }
    }
    settings.configure(DATABASES=MYDB)

import pprint
import csv
makeDatabasesSetting()
from dcdemo.apps.dmodrv.models import Country
from dcdemo.apps.dmodrv.models import City   

from dcdemo.apps.dmodrv.models import Station
from dcdemo.apps.dmodrv.models import WindObservation
from dcdemo.apps.dmodrv.models import TemperatureObservation


from django.conf import settings
from decimal import Decimal
from datetime import datetime

STATIONDATAPATH = """/home/rshea/dev/djchdemo/data/weatherstationdata.csv"""
WINDDATAPATH = """/home/rshea/dev/djchdemo/data/wind.csv"""
TEMPDATAPATH = """/home/rshea/dev/djchdemo/data/screen.csv"""
def removeLineFeed(line):
    '''
    Removes line feeds from the argument supplied
    '''
    line = line.replace("\r","")
    line = line.replace("\n","")
    return line

def processStationData(line_from_file, notUsed):
    '''
    Name,Agent Number,Network Number,Latitude (dec.deg),Longitude (dec.deg),Height (m),Posn_Precision,Observing Authority
    '''
    lstData = line_from_file.split(",")
    nam = lstData[0]
    lat = Decimal(lstData[3])
    lng = Decimal(lstData[4])
    ht = Decimal(lstData[5])
    s = Station(name=nam, longitude=lng, latitude=lat, height_in_m=ht)
    s.save()

def processWindData(line_from_file, notUsed):
    '''
    Station,Date(NZST),Dir(DegT),Speed(km/hr),Dir StdDev,Spd StdDev,Period(Hrs),Freq
    '''
    lstData = line_from_file.split(",")
    s = Station.objects.get(name=lstData[0])
    dt= datetime.strptime(lstData[1] , '%Y%m%d:%H%M') 
    dirctn = Decimal(lstData[2])
    spd_kmh = Decimal(lstData[3])

    wo = WindObservation(station=s, when=dt, speed=spd_kmh, direction=dirctn)
    wo.save()

def processTempData(line_from_file, notUsed):
    '''
    Station,Date(NZST),Tair(C),Twet(C),RH(%),Tdew(C)
    '''
    lstData = line_from_file.split(",")
    s = Station.objects.get(name=lstData[0])
    dt= datetime.strptime(lstData[1] , '%Y%m%d:%H%M') 

    air_temp = Decimal(lstData[2])
    wet_temp = Decimal(lstData[3])
    relhum = Decimal(lstData[4])

    to = TemperatureObservation(station=s, when=dt, air=air_temp, wet=wet_temp, relative_humidity = relhum, dew_point=0) 
    to.save()

def populateAbstract(pathToData, lineProcessingFunction, lpfArg0=None):
    '''
    Opens file on specified path, ignores first line and then
    iterates over all others invoking the 'lineProcessingFunction'
    function each time with the line of input as an argument to it.
    
    The returned objects are saved into a list and at the end of
    processing the input this list is returned to the caller
    '''
    lstOut = []
    firstLine = True
    for line in open(pathToData):
        if firstLine:
            firstLine = False
        else:
            line = removeLineFeed(line)
            if line == None or len(line.strip()) < 1:
                pass
            else:
                lstOut.append(lineProcessingFunction(line, lpfArg0))
    return lstOut

def populateTemperatureObservations(pathToTempData):
    '''
    Invokes populateAbstract to process the input data
    defined in the file at 'pathToTempData'
    '''
    populateAbstract(pathToTempData, processTempData)

def populateWindObservations(pathToWindData):
    '''
    Invokes populateAbstract to process the input data
    defined in the file at 'pathToStationData'
    '''
    populateAbstract(pathToWindData, processWindData)

def populateStations(pathToStationData):
    '''
    Invokes populateAbstract to process the input data
    defined in the file at 'pathToStationData'
    '''
    populateAbstract(pathToStationData, processStationData)

def main():
    Station.objects.all().delete()
    WindObservation.objects.all().delete()
    TemperatureObservation.objects.all().delete()

    lstStations = populateStations(STATIONDATAPATH)
    lstWindObs = populateWindObservations(WINDDATAPATH)
    lstTempObs = populateTemperatureObservations(TEMPDATAPATH)

if __name__ == '__main__':
    main()
