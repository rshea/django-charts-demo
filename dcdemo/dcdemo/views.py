import collections

from django.http import HttpResponse 
from django.shortcuts import render_to_response
from django.template import RequestContext, loader

def sitehome(request):
    '''
    Super temporary site home page
    '''

    Menupick = collections.namedtuple('MenuPick', 'literal url')
    demos = []
    demos.append(Menupick(literal='Django-Graphos', url='dmodjgrap'))
    demos.append(Menupick(literal='ChartIt', url='dmochrtit'))
    demos.append(Menupick(literal='chartkick', url='dmochrkck'))

    sout = "<h1>Django Charts Demo</h1>"
    for d in demos:
        sout += '''<p><a href="%s">%s Demo</a></p>''' % (d.url, d.literal)

    return HttpResponse(sout)
