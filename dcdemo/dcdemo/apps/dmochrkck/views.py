import datetime
import pytz
import pprint

from django.http import HttpResponse 
from django.shortcuts import render_to_response
from django.template import RequestContext, loader
from django.shortcuts import render

from ..dmodrv.models import WindObservation

def index(request):
    template = loader.get_template('dmochrkck/index.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))

def chart1(request):
    '''
    Produce a line chart showing windspeed at an observation
    station over a specificed period of time

    Initially both period of time and choice of observation
    station are hardcoded
    '''

    #Get some wind observations for a fixed period and location
    utc = pytz.utc
    from dcdemo.apps.dmodrv.models import WindObservation
    wo = WindObservation.objects.filter(
            when__gte=datetime.datetime(2005, 1, 1, 0, 0, 0, tzinfo=utc), 
            when__lte=datetime.datetime(2005, 1, 1, 23, 59, 59, tzinfo=utc),
            station_id=1)

    #Build a dictionary of string value keyed by ISO date/times
    exchange = {}
    for windobs in wo:
        exchange[windobs.when.strftime('%Y%m%dT%H%M%S')] = str(windobs.speed) 

    pprint.pprint(exchange)

    template = loader.get_template('dmochrkck/chart1.html')
    context = RequestContext(request, {})
    context["exchange"] = exchange
    return HttpResponse(template.render(context))

