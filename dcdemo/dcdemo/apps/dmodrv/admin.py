from django.contrib import admin

from .models import Country
from .models import City   
from .models import Station
from .models import WindObservation
from .models import TemperatureObservation

admin.site.register(Country)
admin.site.register(City)
admin.site.register(Station)
admin.site.register(WindObservation)
admin.site.register(TemperatureObservation)

