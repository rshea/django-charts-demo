from django.db import models

class Country(models.Model):
    class Meta:
        ordering = ('name', )

    name = models.CharField("country name", max_length=200)
    isocode = models.CharField("country ISO Code",  max_length=2, unique=True)

    def __str__(self):
        return "%s (%s)" % \
                (self.name, self.isocode)

class City(models.Model):
    class Meta:
        ordering = ('country','name', )

    country = models.ForeignKey(Country)
    name = models.CharField("city Name", max_length=200)
    population = models.IntegerField("population", default=0, null=True, blank=True)
    longitude = models.DecimalField("longitude", default=0, max_digits=9, decimal_places=5)
    latitude = models.DecimalField("latitude", default=0, max_digits=9, decimal_places=5)

    def __str__(self):
        if self.population == None:
            pop = "N/A"
        else:
            pop = "%d" % self.population
        if self.longitude == None:
            lng = "N/A"
        else:
            lng = "%f" % self.longitude
        if self.latitude == None:
            lat = "N/A"
        else:
            lat = "%f" % self.latitude

        return "%s, %s . Pop: %s . Long %s, Lat %s" % \
                (self.name, self.country.name, \
                pop, lng, \
                lat)

class Station(models.Model):
    class Meta:
        ordering = ('name', )

    name = models.CharField("station name", max_length=200)
    longitude = models.DecimalField("longitude", default=0, max_digits=9, decimal_places=5)
    latitude = models.DecimalField("latitude", default=0, max_digits=9, decimal_places=5)
    height_in_m = models.DecimalField("height in metres", default=0, max_digits=6, decimal_places=2) 

    def __str__(self):
        return "%s (Long: %.3f, Lat: %.3f)" % \
            (self.name, self.longitude, self.latitude)

class WindObservation(models.Model):
    class Meta:
        ordering = ('station','when', )

    station = models.ForeignKey(Station)
    when = models.DateTimeField()
    speed = models.DecimalField("speed (km/h)", default=0, max_digits=6, decimal_places=3)
    direction = models.IntegerField("direction", default=0)

    def _when_iso(self):
        "Returns the `when` property formatted as an ISO date"
        return str(self.when)

    when_iso = property(_when_iso)

    def __str__(self):
        return "%s %s" % \
            (self.station.name, self.when)

class TemperatureObservation(models.Model):
    class Meta:
        ordering = ('station','when', )

    station = models.ForeignKey(Station)
    when = models.DateTimeField()
    air = models.DecimalField("air temp (c)", default=0, max_digits=6, decimal_places=3)
    wet = models.DecimalField("wet temp (c)", default=0, max_digits=6, decimal_places=3)
    relative_humidity = models.DecimalField("rel humidity (%)", default=0, max_digits=6, decimal_places=3)
    dew_point = models.DecimalField("dew point", default=0, max_digits=6, decimal_places=3)

    def __str__(self):
        return "%s %s" % \
            (self.station.name, self.when)
