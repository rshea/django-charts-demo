from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'index^$', views.index, name='index'),
    url(r'^chart1$', views.chart1, name='chart1')
)
