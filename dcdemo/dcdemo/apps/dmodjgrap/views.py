import datetime
import pytz
import pprint

from django.http import HttpResponse 
from django.shortcuts import render_to_response
from django.template import RequestContext, loader

from graphos.renderers import flot
from graphos.sources.simple import SimpleDataSource

from ..dmodrv.models import WindObservation

def index(request):
    template = loader.get_template('dmodjgrap/index.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))

def chart1(request):
    #Get some wind observations for a fixed period and location
    utc = pytz.utc
    from dcdemo.apps.dmodrv.models import WindObservation
    wo = WindObservation.objects.filter(
            when__gte=datetime.datetime(2005, 1, 1, 0, 0, 0, tzinfo=utc), 
            when__lte=datetime.datetime(2005, 1, 1, 23, 59, 59, tzinfo=utc),
            station_id=1)

    #Build a list of lists; each inner list being a x/y data point 
    chart_data = []
    chart_data.append(['Hour (24H)', 'Speed (Km/h)'])
    for windobs in wo:
        chart_data.append([windobs.when.hour, str(windobs.speed)])

    pprint.pprint(chart_data)

    #The following dictionary illustrates how we can directly
    #provide options to the flot chart which correspond to the
    #normal flot options. We provide a python dict which corresponds
    #to the javascript object which would normally be provided
    #
    #In this case we're ensuring that we don't end up with axis
    #label values which are something other than integers
    opt = {
        'xaxis': {
            'tickDecimals': 0,
        },
        'yaxis': {
            'tickDecimals': 0,
        }
    }

    line_chart = flot.LineChart(SimpleDataSource(data=chart_data), html_id="line_chart", options=opt)
    context = RequestContext(request, {})
    context["line_chart"] = line_chart
    template = loader.get_template('dmodjgrap/chart1.html')
    return HttpResponse(template.render(context))


