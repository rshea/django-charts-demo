# Create your views here.
from django.http import HttpResponse 
from django.shortcuts import render_to_response
from django.template import RequestContext, loader

from chartit import DataPool, Chart

from ..dmodrv.models import WindObservation

def index(request):
    template = loader.get_template('dmochrtit/index.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))
def chart1_old(request):
    chart = weather_chart_view(request)
    template = loader.get_template('dmochrtit/chart1.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))


def chart1(request):


    #import pdb; pdb.set_trace()


    #Step 1: Create a DataPool with the data we want to retrieve.
    weatherdata = \
        DataPool(
            series=
            [{'options': {
            'source': WindObservation.objects.all()},
            'terms': [
            'when_iso',
            'speed']}
        ])

    #Step 2: Create the Chart object
    cht = Chart(
        datasource = weatherdata,
        series_options =
            [{'options':{
            'type': 'line',
            'stacking': False},
            'terms':{
            'when_iso': [
            'speed']
            }}],
        chart_options =
            {'title': {
            'text': 'Weather Data of Boston and Houston'},
            'xAxis': {
            'title': {
            'text': 'Month number'}}})

    #Step 3: Send the chart object to the template.
    return render_to_response('dmochrtit/chart1.html', {'weatherchart': cht})


