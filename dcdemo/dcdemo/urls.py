from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

from dcdemo.views import sitehome
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dcdemo.views.home', name='home'),
    # url(r'^dcdemo/', include('dcdemo.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^dmodjgrap/', include('dcdemo.apps.dmodjgrap.urls')),
    url(r'^dmochrtit/', include('dcdemo.apps.dmochrtit.urls')),
    url(r'^dmochrkck/', include('dcdemo.apps.dmochrkck.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url('^$', sitehome),
)
