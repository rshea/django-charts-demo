django-charts-demo
=========

A django project to demonstrate different approaches to integrating charts with Django

Makes use of "Django Suit" http://djangosuit.com

Candidate Libraries
-------
 - [ChartIt](http://chartit.shutupandship.com/)
 - [chartkick](https://github.com/mher/chartkick.py)
 - [django-graphos](https://github.com/agiliq/django-graphos)


ChartIt
-------
The demo application within this repository is **dmochrtit**

[ChartIt](http://chartit.shutupandship.com/). 

License: The Python code is BSD but django-chartit has dependencies on [jQuery](http://jquery.com/) and [HighCharts](http://www.highcharts.com/products/highcharts). While jQuery is either MIT or LGPL2 HighCharts has the [HightCharts License](http://shop.highsoft.com/highcharts.html) and you have to decide whether you can live with that.

[Full instructions is at](http://chartit.shutupandship.com/docs/)

'30 second' instructions :

 - `pip install django-chartit`
 - `pip install simplejson` (fixes a problem that occurs with some data types)
 - Download HighCharts and jQuery to `static` directory of host application
 - Add 'chartit' to `INSTALLED_APPS` in settings.
 - Add host application to `INSTALLED_APPS` in settings.
 - Add suitable method, eg `chart1`, method to views.py in host application.
 - Revise urls.py to route a url to the views.py method just created.
 - Add template for use by `index` method to host application.


Relevant Defects:
 - (https://github.com/pgollakota/django-chartit/issues/8)
 - (https://github.com/pgollakota/django-chartit/issues/12)

***
Chartkick
-------
The demo application within this repository is **dmochrkck**

License: The Python code is MIT but chartkick has dependencies on [jQuery](http://jquery.com/) and [HighCharts](http://www.highcharts.com/products/highcharts). While jQuery is either MIT or LGPL2 HighCharts has the [HightCharts License](http://shop.highsoft.com/highcharts.html) and you have to decide whether you can live with that.

'30 second' instructions :

 - `pip install chartkick`
 - Download Flot to `static` directory of host application (see my sample code for what I left in from git download)
 - Add 'chartkick' to `INSTALLED_APPS` in settings.
 - Add host application to `INSTALLED_APPS` in settings.
 - Add 'chartkick.js' to STATICFILES _DIRS 
 - Add suitable method, eg `chart1`, method to views.py in host application.
 - Revise urls.py to route a url to the views.py method just created.
 - Add template for use by `index` method to host application.

***
django-graphos
-------
The demo application within this repository is **dmodjgrp**

[django-graphos](http://agiliq.com/docs/django-graphos/index.html). 

[demos for django-graphos](http://agiliq.com/demo/graphos/)

License: The Python code is BSD. django-graphos has dependencies on [jQuery](http://jquery.com/) and jQuery is either MIT or LGPL.

NB: To date my only interest has been in using the [Flot charts](http://www.flotcharts.org/) output from django-graphos. 

'30 second' instructions :

 - `pip install django-graphos`
 - Download Flot to `static` directory of host application (see my sample code for what I left in from git download)
 - Add host application to `INSTALLED_APPS` in settings.
 - Add 'graphos' to `INSTALLED_APPS` in settings.
 - Add suitable method, eg `chart1`, method to views.py in host application.
 - Revise urls.py to route a url to the views.py method just created.
 - Add template for use by `index` method to host application.





